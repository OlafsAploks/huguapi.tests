﻿using HuguAPI.Services.Database;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace HuguAPI.Tests.UnitTests.Database
{
	[TestFixture]
    class DBResultsRowTests
    {
		private DBResultsRow _target;
		
		[SetUp]
		public void Setup()
		{
			_target = new DBResultsRow();
		}
		[Test]
		[TestCase("ID", 10)]
		[TestCase("Age", 10)]
		public void Add_EmptyDictionary_PopulatesDictionaryWithValues(string key, object value)
		{
			_target.Add(key, value);
			Assert.AreEqual(_target.GetInt(key), value);
		}
		[Test]
		[TestCase("Key", "125012501025")]
		[TestCase("Age", 10)]
		[TestCase("IsDeleted", true)]
		public void Add_DictionaryAlreadyHasKey_ThrowsException(string key, object value)
		{
			_target.Add(key, value);
			Assert.Throws<ArgumentException>(new TestDelegate(() => _target.Add(key, value)));
		}
		[Test]
		[TestCase(new string[0], new object[0])]
		[TestCase(new string[3] { "Key", "Age", "IsDeleted" }, new object[3] { "123456789", 13, false })]
		[TestCase(new string[5] { "F1", "F2", "F3", "F4", "F5" }, new object[5] { "aaa", 13, false, "AAA", 'a'})]
		public void GetCount_Dictionary_ReturnsDBColumnCount(string[] key, object[] value)
		{
			for (int i = 0; i < key.Length; i++)
			{
				_target.Add(key[i], value[i]);
			}
			Assert.AreEqual(key.Length, _target.GetCount());
		}
		[Test]
		[TestCase("F1", "Value")]
		[TestCase("F1", true)]
		[TestCase("F1", 'v')]
		public void GetInt_FieldsValueHasDifferentType_ThrowsException(string key, object value)
		{
			_target.Add(key, value);
			Assert.Throws<InvalidCastException>(new TestDelegate(() => _target.GetInt(key)));
		}
		[Test]
		public void GetInt_FieldsValueIsInteger_ReturnValueAsInteger()
		{
			_target.Add("F1", 123);
			Assert.DoesNotThrow(new TestDelegate(() => { _target.GetInt("F1"); }));
		}
		[Test]
		[TestCase("F1", 1)]
		[TestCase("F1", 'a')]
		[TestCase("F1", false)]
		public void GetString_FieldsValuesIsString_ThrowsException(string key, object value)
		{
			_target.Add(key, value);
			Assert.Throws<InvalidCastException>(new TestDelegate(() => _target.GetString(key)));
		}
		[Test]
		public void GetString_FieldsValueIsString_ReturnValueAsString()
		{
			_target.Add("F1", "abcAbc!@#");
			Assert.DoesNotThrow(new TestDelegate(() => { _target.GetString("F1"); }));
		}
		[Test]
		[TestCase("Field1", "abcAbc!@#")]
		[TestCase("Field1", 123)]
		[TestCase("Field1", 'a')]
		[TestCase("Field1", true)]
		public void GetValueFromKey_FieldsValueAreDifferentTypes_ReturnValueAsString(string key, object value)
		{
			_target.Add(key, value);
			Assert.AreEqual(value, _target.GetValueFromKey(key));
		}

	}
}
