﻿using HuguAPI.Models;
using HuguAPI.Services.Database;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace HuguAPI.Tests.UnitTests.Validators
{
	[TestFixture]
    class EntityMappingLibraryTests
    {
		private IDBResultsRow _mockedDBResultsRow;
		private EntityMappingLibrary _target;
		public EntityMappingLibraryTests()
		{
			_target = new EntityMappingLibrary();
		}
		[SetUp]
		public void Setup()
		{
			_mockedDBResultsRow = Substitute.For<IDBResultsRow>();
		}
		[Test]
		[TestCase(123, "abcdefghjijk123456", "HUGU-0001")]
		[TestCase(1, "a", "H")]
		public void MapBear_CorrectMappingParameters_CreatesBearInstance(int id, string key, string name)
		{
			_mockedDBResultsRow.GetInt("Id").Returns(id);
			_mockedDBResultsRow.GetString("BearKey").Returns(key);
			_mockedDBResultsRow.GetString("FactoryName").Returns(name);
			var shouldBe = new Bear
			{
				Id = id,
				BearKey = key,
				FactoryName = name
			};
			var mapedBear = _target.MapBear(_mockedDBResultsRow);
			// Iterates over all class properties and compares property values
			foreach (PropertyInfo propertyInfo in mapedBear.GetType().GetProperties())
			{
				Assert.True(propertyInfo.GetValue(mapedBear).Equals(propertyInfo.GetValue(shouldBe)));
			}
		}
    }
}
