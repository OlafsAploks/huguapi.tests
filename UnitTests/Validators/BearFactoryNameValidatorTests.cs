﻿using HuguAPI.Repositories;
using HuguAPI.Services;
using HuguAPI.Services.Bear;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace HuguAPI.Tests.UnitTests.Validators
{
    class BearFactoryNameValidatorTests
    {
		private IBearFactoryNameValidator target;
		private IBearRepository _subBearRepository;
		[SetUp]
		public void Setup()
		{
			var regexLib = new RegexCollection();
			_subBearRepository = Substitute.For<IBearRepository>();

			//RETURN STATETEMENTS FOR MOCK OBJECT
			target = new BearFactoryNameValidator(regexLib, _subBearRepository);
		}

		[Test]
		[TestCase(null)]
		public void Validate_FactoryNameNoStringGiven_NullExceptionThronw(string factName)
		{
			Assert.Throws<ArgumentNullException>(new TestDelegate(() => target.validate(factName)));
		}

		[Test]
		[TestCase("HUGU-001")]
		[TestCase("HUGU-00001")]
		[TestCase("HUG-001")]
		[TestCase("HUGU001")]
		[TestCase(" HUGU-001")]
		[TestCase("HUGU-001 ")]
		[TestCase("Hugu-001")]
		[TestCase("0001-HUGU")]
		public void Validate_FactoryNameWithInvalidPatternGiven_ExceptionThrown(string factName)
		{
			Assert.Throws<ArgumentException>(new TestDelegate(() => target.validate(factName)));
		}

		[Test]
		[TestCase("HUGU-0001")]
		[TestCase("HUGU-1234")]
		[TestCase("HUGU-5678")]
		[TestCase("HUGU-9999")]
		public void Validate_FactoryNameWithValidPatternGiven_Success(string factName)
		{
			Assert.DoesNotThrow(() => target.validate(factName));
		}
		
		[Test]
		public void Validate_FactoryNameAlreadyRegistered_ThrowsException()
		{
			_subBearRepository.GetByFactoryName("HUGU-1111").Returns(new Models.Bear("Test", "Test"));
			Assert.Throws<ArgumentException>(new TestDelegate(() => target.validate("HUGU-1111")));
		} 

		[Test]
		public void Validate_FactoryNameNotYetRegistered_Success()
		{
			_subBearRepository.GetByFactoryName("HUGU-1111").Returns(a => null);
			Assert.True(target.validate("HUGU-1111"));
		}
	}
}
