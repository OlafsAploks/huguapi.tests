﻿using HuguAPI.Services;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace HuguAPI.Tests.UnitTests.Services.Bear
{
	[TestFixture]
	class BearFactoryTests
    {
		private IBearFactory target;

		[SetUp]
		public void Setup()
		{
			var mockedBearKeyFactory = Substitute.For<IBearKeyFactory>();
			mockedBearKeyFactory.GetKey(20).Returns("1234567890abcdefghij");
			target = new BearFactory(mockedBearKeyFactory);
		}

		[Test]
		[TestCase("Hugu-0001")]
		[TestCase("Hugu-1111")]
		public void Create_WithFactoryName_SetsFactoryName(string userFactoryNameInput)
		{
			var bearInstance = target.createBear(userFactoryNameInput);
			Assert.AreEqual(bearInstance.FactoryName, userFactoryNameInput);
		}
    }
}
