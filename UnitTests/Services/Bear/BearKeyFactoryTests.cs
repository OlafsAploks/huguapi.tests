﻿using HuguAPI.Services;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace HuguAPI.Tests.UnitTests.Services.Bear
{
	[TestFixture]
	class BearKeyFactoryTests
    {
		private IBearKeyFactory target;

		[SetUp]
		public void Setup()
		{
			var mockedRandomFactory = Substitute.For<IRandomFactory>();
			mockedRandomFactory.getNextInt(Arg.Any<int>()).Returns(0);
			target = new BearKeyFactory(mockedRandomFactory);
		}

		[Test]
		[TestCase(10)]
		[TestCase(20)]
		public void GetKey__ReturnsBearKey(int keyLength)
		{
			var key = target.GetKey(keyLength);
			Assert.AreEqual(keyLength, key.Length);
		}

	}
}
